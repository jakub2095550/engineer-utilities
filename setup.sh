#!/usr/bin/env bash
set -e

APT_UPDATED=
LOG_DIR=/tmp/setup-logs
PROCESS_LOG="$LOG_DIR/process.log"
mkdir -p $LOG_DIR

RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
ENDCOLOR="\e[0m"

function echo_red {
    echo -e "${RED}$@${ENDCOLOR}"
}
function echo_yellow {
    echo -e "${YELLOW}$@${ENDCOLOR}"
}
function echo_green {
    echo -e "${GREEN}$@${ENDCOLOR}"
}

function run_process {
    local command="$1"
    #log_file="$LOG_DIR/$(echo "$command" | md5sum | cut -f1 -d' ').log"
    log_file="$PROCESS_LOG" # We only need to have one log file...
    printf "${YELLOW}RUNNING: $command: "
    if $command &>"$log_file"
    then
        echo_green "SUCCESS"
        rm "$log_file"
    else
        echo_red "FAILED: $log_file"
        exit 1
    fi
}

function add_to_bashrc_begin {
    >~/.bashrc.setup-script
    if ! grep -q .bashrc.setup-script ~/.bashrc
    then
        echo ". ~/.bashrc.setup-script" >> ~/.bashrc
    fi
}
function add_to_bashrc {
    printf "%s\n" "$@" >>~/.bashrc.setup-script
    printf "BASHRC: %s\n" "$@"
}

function test_sudo {
    sudo echo -n
}

function install_exe {
    local exe=$1
    local package=${2:-$exe}
    local manager=${3:-apt}
    local install_opts=${4:--y}
    if which $exe &>/dev/null
    then
        echo_green "Already installed $exe ($package)"
    else
        echo_yellow "Installing $exe ($package)"
        test_sudo
        if [[ "$manager" == "apt" && ! "$APT_UPDATED" ]]
        then
            run_process "sudo apt update"
        fi
        run_process "sudo $manager install $install_opts $package"
    fi
}

for exe in vim git htop python3 curl
do
    install_exe $exe
done
install_exe gpg gnupg
install_exe pip3 python3-pip
install_exe update-ca-certificates ca-certificates
install_exe terraform terraform snap --classic

run_process "pip3 install --user --break-system-package -r requirements.txt"

if [[ ! -f /etc/apt/sources.list.d/docker.list ]]
then
    echo_yellow "Setting up the docker registry..."
    test_sudo
    sudo install -m 0755 -d /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor --batch --yes -o /etc/apt/keyrings/docker.gpg
    sudo chmod a+r /etc/apt/keyrings/docker.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
        $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
        sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    APT_UPDATED=
fi
install_exe docker "docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin"
if ! groups | grep -q docker
then
    echo_yellow "Adding you to the docker group"
    sudo usermod -aG docker $(whoami)
    echo_red "You need to login again for this change to take effect!"
fi

if ! which docker-compose &>/dev/null
then
    echo_yellow "Setting up docker-compose"
    test_sudo
    sudo curl -sSL "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi


add_to_bashrc_begin
add_to_bashrc 'if ! echo "$PATH"|grep -q "\.local/bin"; then export PATH=~/.local/bin:${PATH}; fi'

echo_green FINISHED
