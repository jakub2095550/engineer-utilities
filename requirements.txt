ansible
ansible-lint
molecule
molecule-docker
molecule-inspec
pytest-testinfra
yamllint
# The below dependencies need to be specifically specified and locked down due to a compatibility bug
pyyaml==5.3.1
cython<3.0
