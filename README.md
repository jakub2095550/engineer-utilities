# Engineer Utilities

A collection of utitities for the enginineers, including setup steps.

[TOC]

## Local Development Environment Setup

Run this script to setup all the tools you may need to get started.

```
./setup.sh
```

## VPN Setup

To setup your VPN you need to have a username and password to access the VPN server.
You can download the VPN config files from the VPN website at: https://vpn.windsor.ai
Then you can setup the VPN on your OS by following the setps below.

### Ubuntu Linux

1. Go to `Settings > Network` and click on the `+` icon for VPN connection.
1. Select `Import from file...` and select the `.ovpn` file from the `.zip` file downloaded from https://vpn.windsor.ai.
1. Give it a meaningful name and provide your own username and password credentials shared with you.
1. Tick `Use this connection only for resources on its network` under the `IPv4` and `IPv6` tabs.

You now have a VPN toggle next to your quick access network settings in the top right of your desktop (power button).
